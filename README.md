# sisop-praktikum-modul-3-2023-AM-C04
Terdiri dari:
- Alfadito Aulia Denova | 5025211157 
- Layyinatul Fuadah | 5025211207 
- Armadya Hermawan S | 5025211243 

---

## Penjelasan Soal dan Dokumentasi
### Soal 1
```c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <ctype.h>

char send[26];

struct MinHeapNode
{
   char data;          
   unsigned freq;        
   struct MinHeapNode* left;
   struct MinHeapNode* right;
};
```
`newNode`: Fungsi ini digunakan untuk membuat dan menginisialisasi node baru dalam pohon Huffman. Node ini memiliki data karakter, frekuensi kemunculan, serta pointer ke node anak kiri dan kanan.
```c
struct MinHeap {
  unsigned size;
  unsigned capacity;
  struct MinHeapNode **array;
};
struct MinHeapNode *newNode(char data, unsigned freq) {
  struct MinHeapNode *temp = (struct MinHeapNode *)malloc(sizeof(struct MinHeapNode));

  temp->left = temp->right = NULL;
  temp->data = data;
  temp->freq = freq;

  return temp;
}
struct MinHeap *createMinH(unsigned capacity) {
  struct MinHeap *minHeap = (struct MinHeap *)malloc(sizeof(struct MinHeap));

  minHeap->size = 0;

  minHeap->capacity = capacity;

  minHeap->array = (struct MinHeapNode **)malloc(minHeap->capacity * sizeof(struct MinHeapNode *));
  return minHeap;
}
```
`createMinH`: Fungsi ini digunakan untuk membuat dan menginisialisasi min heap yang akan digunakan dalam algoritma Huffman. Min heap ini akan menyimpan pointer ke node-node dalam pohon Huffman.
```c
void swapMinHeapNode(struct MinHeapNode **a, struct MinHeapNode **b) {
  struct MinHeapNode *t = *a;
  *a = *b;
  *b = t;
}
```
`swapMinHeapNode`: Fungsi ini digunakan untuk menukar posisi dua node dalam min heap.
```c
void minHeapify(struct MinHeap *minHeap, int idx) {
  int smallest = idx;
  int left = 2 * idx + 1;
  int right = 2 * idx + 2;

  if (left < minHeap->size && minHeap->array[left]->freq < minHeap->array[smallest]->freq)
    smallest = left;

  if (right < minHeap->size && minHeap->array[right]->freq < minHeap->array[smallest]->freq)
    smallest = right;

  if (smallest != idx) {
    swapMinHeapNode(&minHeap->array[smallest], &minHeap->array[idx]);
    minHeapify(minHeap, smallest);
  }
}
```
`minHeapify`: Fungsi ini digunakan untuk memperbaiki min heap jika ada node yang melanggar aturan min heap setelah operasi penambahan atau penghapusan node.

```c
int checkSizeOne(struct MinHeap *minHeap) {
  return (minHeap->size == 1);
}
```
`checkSizeOne`: Fungsi ini digunakan untuk memeriksa apakah ukuran min heap hanya satu.
```c
struct MinHeapNode *extractMin(struct MinHeap *minHeap) {
  struct MinHeapNode *temp = minHeap->array[0];
  minHeap->array[0] = minHeap->array[minHeap->size - 1];

  --minHeap->size;
  minHeapify(minHeap, 0);

  return temp;
}
```
`extractMin`: Fungsi ini digunakan untuk mengeluarkan node dengan frekuensi terendah dari min heap
```c
void insertMinHeap(struct MinHeap *minHeap, struct MinHeapNode *minHeapNode) {
  ++minHeap->size;
  int i = minHeap->size - 1;

  while (i && minHeapNode->freq < minHeap->array[(i - 1) / 2]->freq) {
    minHeap->array[i] = minHeap->array[(i - 1) / 2];
    i = (i - 1) / 2;
  }
  minHeap->array[i] = minHeapNode;
}
```
insertMinHeap`: Fungsi ini digunakan untuk menyisipkan node baru ke dalam min heap dan memastikan aturan min heap tetap terjaga.
`

```c
void buildMinHeap(struct MinHeap *minHeap) {
  int n = minHeap->size - 1;
  int i;

  for (i = (n - 1) / 2; i >= 0; --i)
    minHeapify(minHeap, i);
}
```
`buildMinHeap`: Fungsi ini digunakan untuk membangun min heap dari array node-node yang ada.

```c
int isLeaf(struct MinHeapNode *root) {
  return !(root->left) && !(root->right);
}
```
`isLeaf`: Fungsi ini digunakan untuk memeriksa apakah sebuah node adalah daun dalam pohon Huffman.

```c
struct MinHeap *createAndBuildMinHeap(char data[], int freq[], int size) {
  struct MinHeap *minHeap = createMinH(size);

  for (int i = 0; i < size; ++i)
    minHeap->array[i] = newNode(data[i], freq[i]);

  minHeap->size = size;
  buildMinHeap(minHeap);

  return minHeap;
}
```
`createAndBuildMinHeap`: Fungsi ini digunakan untuk membuat dan membangun min heap berdasarkan data karakter dan frekuensi kemunculannya.

```c
struct MinHeapNode *buildHuffmanTree(char data[], int freq[], int size) {
  struct MinHeapNode *left, *right, *top;
  struct MinHeap *minHeap = createAndBuildMinHeap(data, freq, size);

  while (!checkSizeOne(minHeap)) {
    left = extractMin(minHeap);
    right = extractMin(minHeap);

    top = newNode('$', left->freq + right->freq);

    top->left = left;
    top->right = right;

    insertMinHeap(minHeap, top);
  }
  return extractMin(minHeap);
}
```
`buildHuffmanTree`: Fungsi ini digunakan untuk membangun pohon Huffman berdasarkan data karakter dan frekuensi kemunculannya.

```c
void printArray(int arr[], int n, char c) {
    FILE *fp = fopen("temp.txt", "a");

    if (fp == NULL) {
        printf("Error opening file\n");
        return;
    }

    fprintf(fp, "%c", c);
    for (int i = 0; i < n; ++i) {
        fprintf(fp, "%d", arr[i]);
    }
    fprintf(fp, "%c", '\n');

    fclose(fp);
}
```
`printArray`: Fungsi ini digunakan untuk mencetak kode Huffman untuk karakter tertentu ke dalam file temp.txt.

```c
void printHCodes(struct MinHeapNode *root, int arr[], int top) {
  if (root->left) {
    arr[top] = 0;
    printHCodes(root->left, arr, top + 1);
  }
  if (root->right) {
    arr[top] = 1;
    printHCodes(root->right, arr, top + 1);
  }
  if (isLeaf(root) && strchr(send, root->data)!=NULL) {
    printArray(arr, top, root->data);
  }
}
```
`printHCodes`: Fungsi ini digunakan untuk mencetak semua kode Huffman dalam pohon Huffman dan menyimpannya ke dalam file temp.txt.

```c
void HuffmanCodes(char data[], int freq[], int size) {
  struct MinHeapNode *root = buildHuffmanTree(data, freq, size);

  int arr[50], top = 0;

  printHCodes(root, arr, top);
}
```
`HuffmanCodes`: Fungsi ini merupakan fungsi utama untuk melakukan kompresi menggunakan algoritma Huffman. Fungsi ini membangun pohon Huffman, mencetak kode Huffman, dan menyimpannya ke dalam file temp.txt.

```c
int count_line(char filename[]){
    int count = 0;
    char line[512];
    FILE *fp = fopen(filename, "r");

    if (fp == NULL) {
        printf("Error opening file.\n");
        return 1;
    }

    while (fgets(line, 512, fp) != NULL) {
        count++;
    }

    fclose(fp);

    return count;
}
```
`count_line`: Fungsi ini digunakan untuk menghitung jumlah baris dalam sebuah file.


```c
int findLength(char c){
FILE *fp = fopen("temp.txt", "r");
if (fp == NULL) {
    printf("Error opening file.\n");
    return 1;
}
char line[512];
while (fgets(line, 512, fp) != NULL) {
    if (line[0] == c) {
        fclose(fp);
        return strlen(line) - 1;
    }
}

fclose(fp);
return -1;
}
```
`findLength`: Fungsi ini digunakan untuk mencari panjang kode Huffman untuk karakter tertentu dalam file temp.txt.
```c

int translate(char filename[]) {
    FILE *fp = fopen(filename, "a");
    FILE *fs = fopen("temp.txt", "r");
    FILE *fc = fopen("file.txt", "r");

if (fp == NULL || fs == NULL || fc == NULL) {
    printf("Error opening file.\n");
    return 1;
}

char line[512];
char c;
int count = 0;
while ((c = fgetc(fc)) != EOF) {
    fseek(fs, 0, SEEK_SET);
    c = toupper(c);
    int found = 0; // Flag to indicate if character c is found
    while (fgets(line, 512, fs) != NULL) {
        if (line[0] == c) {
            found = 1;
            break;
        }
    }
    if (found) {
        count += strlen(line) - 2;
        fwrite(line + 1, strlen(line) - 2, 1, fp);
    }
}

fclose(fp);
fclose(fs);
fclose(fc);
return count;
}
```
`translate`: Fungsi ini digunakan untuk melakukan translasi karakter dalam file input file.txt menjadi kode Huffman yang sesuai, dan menyimpan hasil translasi ke dalam file output translated.txt.

```c
int main()
{
    int parent_to_child_pipe[2];
    int child_to_parent_pipe[2];

    if (pipe(parent_to_child_pipe) == -1 || pipe(child_to_parent_pipe) == -1)
    {
        fprintf(stderr, "Pipe failed\n");
        return 1;
    }

    pid_t pid = fork();
    if (pid == 0)
    {
        // Child process
        close(parent_to_child_pipe[1]);
        close(child_to_parent_pipe[0]);

        // Read data from parent
        char arr[26];
        int arrint[26];
        char sent[26];
        read(parent_to_child_pipe[0], arr, sizeof(arr));
        read(parent_to_child_pipe[0], arrint, sizeof(arrint));
        read(parent_to_child_pipe[0], sent, sizeof(sent));
        close(parent_to_child_pipe[0]);

        // Copy 'sent' to 'send'
        strcpy(send, sent);

        // Perform Huffman encoding
        HuffmanCodes(arr, arrint, sizeof(arr));

        // Write data to parent
        int countLine = count_line("temp.txt");
        write(child_to_parent_pipe[1], &countLine, sizeof(countLine));
        FILE *fp = fopen("temp.txt", "r");
        if (fp == NULL)
        {
            perror("Failed to open file");
            exit(1);
        }
        char line[512];
        while (fgets(line, 512, fp) != NULL)
        {
            write(child_to_parent_pipe[1], &line, sizeof(line));
        }
        fclose(fp);
        close(child_to_parent_pipe[1]);
        exit(0);
    }
    else if (pid > 0)
    {
        // Parent process
        close(parent_to_child_pipe[0]);
        close(child_to_parent_pipe[1]);

        FILE *fp;
        char filename[100];
        char c;

        fp = fopen("file.txt", "r");
        if (fp == NULL)
        {
            printf("Error opening file\n");
            return 1;
        }

        int freq[26] = {0};
        char alpha[26] = {'\0'};
        int idx = 0;
        while ((c = fgetc(fp)) != EOF)
        {
            if (isalpha(c))
            {
                c = toupper(c);
                int pos = c - 'A';
                freq[pos]++;
                if (freq[pos] == 1)
                {
                    alpha[idx] = c;
                    idx++;
                }
            }
        }

        char arr[26];
        int arrfr[26];

        int j = 0;
        for (int i = 0; i < 26; i++)
        {
            alpha[i] = 'A' + i;
            arrfr[i] = freq[i];

            if (freq[i] != 0)
            {
                send[j] = alpha[i];
                j++;
            }
        }

        for (int i = 0; i < 26; i++)
        {
            arr[i] = alpha[i];
        }

        fclose(fp);

        write(parent_to_child_pipe[1], arr, sizeof(arr));
        write(parent_to_child_pipe[1], arrfr, sizeof(arrfr));
        write(parent_to_child_pipe[1], send, sizeof(send));
        close(parent_to_child_pipe[1]);

        int n;
        read(child_to_parent_pipe[0], &n, sizeof(int));

        char coded[n][512];
        for (int i = 0; i < n; i++)
        {
            read(child_to_parent_pipe[0], coded[i], sizeof(char) * 512);
        }
               close(child_to_parent_pipe[0]);

        int number = 0;
        for (int i = 0; i < 26; i++)
        {
            int calc;
            if (freq[i] != 0)
            {
                calc = freq[i] * 8;
                number += calc;
            }
        }

        printf("Jumlah bit sebelum dilakukan kompresi adalah %d\n", number);

        number = translate("translated.txt");

        printf("Jumlah bit sesudah dilakukan kompresi adalah %d\n", number);

        system("rm temp.txt");
    }
    else
    {
        // Fork failed
        fprintf(stderr, "Fork failed\n");
        return 1;
    }

    return 0;
}

```
`main`: Fungsi utama program. Fungsi ini menggunakan dua pipe dan fork untuk mengatur komunikasi antara parent process dan child process. Pada child process, dilakukan kompresi menggunakan algoritma Huffman dan hasilnya dikirim kembali ke parent process. Pada parent process, dilakukan baca tulis data serta perhitungan dan tampilan jumlah bit sebelum dan setelah kompresi.
---
### Soal 2
Pada soal ini diminta untuk melakukan operasi perkalian pada sebuah matriks berukuran 4x2 dan 2x5. Hasil dari matriks tersebut kemudian harus di pass ke program lain dengan menggunakan prinsip Shared memory. Kemudian, melakukan operasi faktorial dengan menggunakan multi-threading, dan membandingkannya dengan cara non-multi threading.

untuk menyelesaikan soal ini, diminta untuk kembuat tiga buah file yaitu kalian.c , cinta.c dan sisop.c.

kalian.c akan mengerjakan bagian perkalian matriksnya, cinta.c akan menerima hasil perkalian tersebut dan melakukan factorial dengan multi threading. sementara sisop.c juga akan mengambil hasil perkalian matriks dan melakukan operasi faktorial, tetapi tanpa multi threading. 

untuk melakukan perbandingan diantara cinta.c dan sisop.c saya mencoba menggunakan waktu sebagai parameter pembandingnya. 

bagian yang digunakan untuk menghitung waktu adalah
```c
clock_t start, end;
    double cpu_time_used;
	start = clock();

```
diawal

dan 
```c
end = clock();
	cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
	printf("\nMulti-threading took %f seconds \n", cpu_time_used);

```
diakhir

dan untuk membaca shared memory maka dibuat lah code berikut pada kalian.c

```c
 /* Allocate shared memory segment */
    key_t key = 1928;
    const int shareSize = sizeof(int) * 4 * 5;  // size of Carr array
    int segmentId = shmget(key, shareSize, IPC_CREAT | 0666);

    /* Attach the shared memory segment */
    int *sharedCarr = (int *)shmat(segmentId, NULL, 0);

    /* Copy the contents of Carr to shared memory */
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            sharedCarr[i * 5 + j] = Carr[i][j];
        }
    }

    /* Rest of your code... */
	sleep(5);
    /* Detach shared memory segment */
    shmdt(sharedCarr);
    /* Remove shared memory segment */
    shmctl(segmentId, IPC_RMID, NULL);


```

dan untuk membaca dari shared memorynya dibuat potongan code berikut
```c
 int segmentId;
    int *sharedCarr;
    key_t key = 1928;     

    /* Allocate shared memory segment */
    const int shareSize = sizeof(int) * 4 * 5;  // size of Carr array
    segmentId = shmget(key, shareSize, IPC_CREAT | 0666);

    /* Attach the shared memory segment */
    sharedCarr = (int *)shmat(segmentId, NULL, 0);

```
tidak lupa kita tetap menutup shared memory setelah diakses.
```c
    /* Detach shared memory segment */
    shmdt(sharedCarr);
    /* Remove shared memory segment */
    shmctl(segmentId, IPC_RMID, NULL);
    
```

Pembuatan threading pada cinta.c memanfaatkan 'pthread.h` dan menggunakan sebuah struct data baru disebut param dan fungsi thread_func yang akan melakukan fungsi factorial biasa secara rekursif

```c
struct param_t {
	int num;
	unsigned long long result;
};

unsigned long long factorial(unsigned long long n){
    if (n >=1)
    	return n * factorial(n-1);
    else
    	return 1;
}

void *thread_func(void *args){
    struct param_t *param = (struct param_t *)args;
    param->result = factorial(param->num);
    return (void *)NULL;
}

```
penggunaannya dalam program main adalah sebagai berikut:
```c
 int rows = sizeof(Carr) / sizeof(Carr[0]);
    int cols = sizeof(Carr[0]) / sizeof(Carr[0][0]);
    pthread_t threads[rows][cols];
    struct param_t params[rows][cols];
    unsigned long long fact[rows][cols];
    
    for (int i=0; i<rows; i++) {
    	for (int j =0; j < cols; j++) {
    	   params[i][j].num = Carr[i][j];
    	   pthread_create(&threads[i][j], NULL, thread_func, &params[i][j]);
    	}
    }
    
    for (int i=0; i<rows; i++){
    	for(int j=0; j<cols; j++){
    	   pthread_join(threads[i][j], NULL);
    	   fact[i][j] = params[i][j].result;
    	   printf("%lld ", fact[i][j]);
       }
    }

```
disini saya curiga bahwa bagian inilah yang memberikan kontribusi waktu paling banyak pada program tersebut. Pada percobaan yang saya lakukan sebanyak 15 kali run, dengan dataset yang berbeda tiap kalinya, didapatkan rata-rata waktu yang dibutuhkan program dengan multi threading adalah 0,0037462 detik.

Sementara, waktu rata-rata untuk waktu yang dibutuhkan program tanpa multi threading adalah 0,0001439 detik. Padahal fungsi rekursif untuk faktorial sama. 

---



### Soal 3

---

### Soal 4
Pada soal nomor 4 dijelaskan bahwa Amin, seorang mahasiswa Informatika mendapati suatu file bernama hehe.zip. Di dalam file .zip tersebut, terdapat sebuah folder bernama files dan file .txt bernama extensions.txt dan max.txt. Setelah melamun beberapa saat, Amin mendapatkan ide untuk merepotkan kalian dengan file tersebut!
#### Point A
Download dan unzip file tersebut dalam kode c bernama unzip.c.
```C
int main() {

    int downloadStatus;
    pid_t downloadPID = fork();

    if (downloadPID < 0) {
        exit(EXIT_FAILURE);
    }

    if (downloadPID == 0) {
        char * cmd_download = "wget \"https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download\" -O hehe.zip NULL";
        system(cmd_download);
    }

    waitpid(downloadPID, & downloadStatus, 0);
```
Perintah wget yang digunakan di dalam child process akan mendownload file dari URL yang telah ditentukan dan menyimpannya dengan nama "hehe.zip" di direktori tempat program dijalankan.
```C
int unzipStat;
    pid_t unzipPid = fork();

    if (unzipPid == 0) {
        char * argv[] = {
            "unzip",
            "-q",
            "hehe.zip",
            NULL
        };
        execv("/usr/bin/unzip", argv);
    }
    waitpid(unzipPid, & unzipStat, 0);

    return 0;
}
```
Perintah unzip yang digunakan di dalam child process akan mengekstrak file "hehe.zip" yang telah didownload di direktori tempat program dijalankan
#### Point B
Selanjutnya, buatlah program categorize.c untuk mengumpulkan (move / copy) file sesuai extension-nya. Extension yang ingin dikumpulkan terdapat dalam file extensions.txt. Buatlah folder categorized dan di dalamnya setiap extension akan dibuatkan folder dengan nama sesuai nama extension-nya dengan nama folder semua lowercase. Akan tetapi, file bisa saja tidak semua lowercase. File lain dengan extension selain yang terdapat dalam .txt files tersebut akan dimasukkan ke folder other.
Pada file max.txt, terdapat angka yang merupakan isi maksimum dari folder tiap extension kecuali folder other. Sehingga, jika penuh, buatlah folder baru dengan format extension (2), extension (3), dan seterusnya.
```C
char extensions[100][100];   // list extension
int extensionsFolder[100];   // jumlah folder dengan extension tertentu
int extensionsCount[100];   // jumlah file tiap extension
int totalExtensions = 0;       // jumlah extension pada file extensions.txt
int maxFiles;                      // max file tiap folder dari max.txt

int main() {

    ///   Initialize Array   ///

    for (int i = 0; i < 100; i++) {
        extensionsFolder[i] = 1;
        extensionsCount[i] = 0;
    }
}
```
Kode program tersebut merupakan inisialisasi array yang akan digunakan dalam program untuk menyimpan informasi tentang ekstensi file dan jumlah folder serta jumlah file yang memiliki ekstensi tertentu.
```C
int main() {

    ///  Get Extensions from extensions.txt ///

    FILE * fileExtensions = fopen("extensions.txt", "r");

    char buffer[100];

    while (fgets(buffer, 100, fileExtensions) != NULL) {
        int len = strcspn(buffer, "\r\n"); // find position of newline or carriage return character
        buffer[len] = '\0';
        strcpy(extensions[totalExtensions], buffer);
        totalExtensions++;
    }

    fclose(fileExtensions);
}
```
Kode program tersebut bertujuan untuk membaca isi file extensions.txt dan menyimpan ekstensi-ekstensi file yang terdapat di dalamnya ke dalam array extensions.
```C
void sortExtensions(char arr[][100], int n) {
    char temp[100];
    for (int i = 0; i < n - 1; i++) {
        for (int j = 0; j < n - i - 1; j++) {
            if (strcmp(arr[j], arr[j + 1]) > 0) {
                strcpy(temp, arr[j]);
                strcpy(arr[j], arr[j + 1]);
                strcpy(arr[j + 1], temp);
            }
        }
    }
}

int main(){
    
    /// Sort Extension in Ascending order    ///

    sortExtensions(extensions, totalExtensions);
}
```
melakukan sorting pada array extensions yang berisi daftar ekstensi file. Fungsi sortExtensions() didefinisikan untuk melakukan sorting tersebut.
```C
int main(){
     /// Add other extensions    ///

    strcpy(extensions[totalExtensions], "other");
    totalExtensions++;
}
```
tambah extension other untuk file extension selain extension pada list
```C
int main(){
    /// Get max files number of files from max.txt ///

    FILE * fileMax = fopen("max.txt", "r");

    fgets(buffer, 100, fileMax);
    maxFiles = atoi(buffer);

    fclose(fileMax);
}
```
Kode program tersebut berfungsi untuk membaca nilai maksimum jumlah file dari file max.txt.
 ```C
void createCategorizedDirectory() {
    system("mkdir categorized");
    logMade("categorized");

    system("cd categorized && mkdir other");
    logMade("other");

    for (int i = 0; i < totalExtensions - 1; i++) {
        char command[255];
        sprintf(command, "mkdir -p categorized/%s", extensions[i]);
        system(command);
        logMade(extensions[i]);
    }
}

int main(){
    ///   Create Categorized Dircetory   ///

    createCategorizedDirectory();
}
```
Buat folder categorized, dan subfolder extensions di dalam folder categorized:
```C
// cek apakah extension file ada pada list extension
int isFound(char extension[]) {
    int isFound = 0;
    for (int i = 0; i < totalExtensions; i++) {
        if (strcmp(extension, extensions[i]) == 0) {
            isFound = 1;
        }
    }

    return isFound;
}

// membuat extension menjadi lowercase
void toLowercase(char * str) {
    int i;
    for (i = 0; str[i] != '\0'; i++) {
        str[i] = tolower(str[i]);
    }
}

// untuk menambahkan quotation mark pada path file agar bisa dieksekusi perintah cp pada system()
char * encloseStringWithQuotes(char * str) {
    size_t len = strlen(str);
    char * newStr = malloc(len + 3);
    if (newStr == NULL) {
        fprintf(stderr, "Error: failed to allocate memory\n");
        return NULL;
    }
    newStr[0] = '\'';
    strcpy( & newStr[1], str);
    newStr[len + 1] = '\'';
    newStr[len + 2] = '\0';
    return newStr;
}

// dapat jumlah file yang ada pada folder
int count_files(const char * path) {
    DIR * dir;
    struct dirent * entry;
    int count = 0;

    if ((dir = opendir(path)) == NULL) {
        perror("opendir() error");
        return -1;
    }

    while ((entry = readdir(dir)) != NULL) {
        if (entry -> d_type == DT_REG) {
            count++;
        }
    }

    closedir(dir);
    return count;
}

// dapatkan index array extensi
int getExtIndex(char * ext) {
    for (int i = 0; i < totalExtensions; i++) {
        if (strcmp(ext, extensions[i]) == 0) {
            return i;
        }
    }

    return -1;
}

// routine prototype
void * categorized_thread(void * arg);

// categorize function
void categorized(const char * path) {
    DIR * directory = opendir(path);

    logAccessed(path);

    if (directory == NULL) {
        printf("Could not open directory: %s\n", path);
        return;
    }

    struct dirent * entry;
    while ((entry = readdir(directory)) != NULL) {
        if (strcmp(entry -> d_name, ".") == 0 || strcmp(entry -> d_name, "..") == 0) {
            continue;
        }

        char fullPath[1024];
        snprintf(fullPath, sizeof(fullPath), "%s/%s", path, entry -> d_name);

        if (entry -> d_type == DT_DIR) {
            // printf("Directory: %s\n", fullPath);
            pthread_t tid;
            pthread_create( & tid, NULL, categorized_thread, fullPath);
            pthread_join(tid, NULL);
        } else {
            char * ext = strrchr(entry -> d_name, '.');
            if (ext) {
                ext = ext + 1;
                toLowercase(ext);
            } else ext = "";
            // printf("%s ", fullPath);

            char * newFullPath = encloseStringWithQuotes(fullPath);

            if (isFound(ext)) {
                int extIndex = getExtIndex(ext);
                extensionsCount[extIndex]++;
                // printf("%s %d\n", ext, extIndex);

                if (extensionsFolder[extIndex] == 1) {
                    char dest[100] = "./categorized/";
                    strcat(dest, ext);

                    int n = count_files(dest);

                    if (n < maxFiles - 1) {
                        char command[100] = "cp ";
                        strcat(command, newFullPath);
                        strcat(command, " ");
                        strcat(command, dest);

                        // printf("%s\n", command);
                        system(command);
                        logMoved(ext, fullPath, ext);
                    } else {
                        extensionsFolder[extIndex]++;

                        char cmd_mkdir[100] = "cd categorized && mkdir ";
                        strcat(cmd_mkdir, ext);

                        char num[10];
                        sprintf(num, "%d", extensionsFolder[extIndex]);
                        strcat(cmd_mkdir, num);

                        system(cmd_mkdir);
                        // printf("%s\n", cmd_mkdir);
                        char folderName[20];
                        folderName[0] = '\0';
                        strcat(folderName, ext);
                        strcat(folderName, num);
                        logMade(folderName);

                        char command[100] = "cp ";
                        strcat(command, newFullPath);
                        strcat(command, " ");
                        strcat(command, dest);

                        // printf("%s\n", command);
                        system(command);

                        char temp[100];
                        strcpy(temp, dest + 14);

                        logMoved(ext, fullPath, temp);

                    }
                } else {
                    char dest[100] = "./categorized/";
                    strcat(dest, ext);

                    char num[10];
                    sprintf(num, "%d", extensionsFolder[extIndex]);
                    strcat(dest, num);

                    int n = count_files(dest);

                    if (n < maxFiles - 1) {
                        char command[100] = "cp ";
                        strcat(command, newFullPath);
                        strcat(command, " ");
                        strcat(command, dest);

                        // printf("%s\n", command);
                        system(command);

                        char temp[100];
                        strcpy(temp, dest + 14);

                        logMoved(ext, fullPath, temp);

                    } else {
                        extensionsFolder[extIndex]++;

                        char cmd_mkdir[100] = "cd categorized && mkdir ";
                        strcat(cmd_mkdir, ext);

                        char num[10];
                        sprintf(num, "%d", extensionsFolder[extIndex]);
                        strcat(cmd_mkdir, num);

                        // printf("%s\n", cmd_mkdir);
                        system(cmd_mkdir);
                        char folderName[20];
                        folderName[0] = '\0';
                        strcat(folderName, ext);
                        strcat(folderName, num);
                        logMade(folderName);

                        char command[100] = "cp ";
                        strcat(command, newFullPath);
                        strcat(command, " ");
                        strcat(command, dest);

                        // printf("%s\n", command);
                        system(command);
                        char temp[100];
                        strcpy(temp, dest + 14);

                        logMoved(ext, fullPath, temp);

                    }
                }
            } else {
                extensionsCount[totalExtensions - 1]++;
                char command[100] = "cp ";
                strcat(command, newFullPath);
                strcat(command, " \"./categorized/other\"");

                // printf("%s\n", command);
                system(command);
                logMoved(ext, fullPath, "other");
            }
        }
    }

    closedir(directory);
}

// routine
void * categorized_thread(void * arg) {
    const char * path = (const char * ) arg;
    categorized(path);
    pthread_exit(NULL);
}

int main(){
    ///   Categorize files   ///

    pthread_t tid;
    pthread_create( & tid, NULL, categorized_thread, "./files");
    pthread_join(tid, NULL);
}
```
•	isFound(char extension[]): memeriksa apakah ekstensi yang diberikan ada dalam daftar ekstensi yang valid.
•	toLowercase(char * str): mengubah string menjadi huruf kecil.
•	encloseStringWithQuotes(char * str): menambahkan tanda kutip tunggal ke awal dan akhir string.
•	count_files(const char * path): mengembalikan jumlah file dalam direktori.
•	getExtIndex(char * ext): mengembalikan indeks ekstensi tertentu dalam daftar ekstensi yang valid.
•	categorized(const char * path): fungsi utama yang mengkategorikan file dalam direktori.

#### Point C
Output-kan pada terminal banyaknya file tiap extension terurut ascending dengan semua lowercase, beserta other juga dengan format sebagai berikut.
extension_a : banyak_file
extension_b : banyak_file
extension_c : banyak_file
other : banyak_file
```C
int main(){
    for (int i = 0; i < totalExtensions; i++) {
        printf("%s: %d\n", extensions[i], extensionsCount[i]);
    }
}
```
#### Point D
Setiap pengaksesan folder, sub-folder, dan semua folder pada program categorize.c wajib menggunakan multithreading. Jika tidak menggunakan akan ada pengurangan nilai.
```C
// routine prototype
void * categorized_thread(void * arg);

void categorized(const char * path) {
    DIR * directory = opendir(path);

    logAccessed(path);

    if (directory == NULL) {
        printf("Could not open directory: %s\n", path);
        return;
    }

    struct dirent * entry;
    while ((entry = readdir(directory)) != NULL) {
        if (strcmp(entry -> d_name, ".") == 0 || strcmp(entry -> d_name, "..") == 0) {
            continue;
        }

        char fullPath[1024];
        snprintf(fullPath, sizeof(fullPath), "%s/%s", path, entry -> d_name);

        if (entry -> d_type == DT_DIR) {
            // buat thread baru ketika melakukan rekursi saat membuka tiap sub folder
        
            pthread_t tid;
            pthread_create( & tid, NULL, categorized_thread, fullPath);
            pthread_join(tid, NULL);
        } else {
            // rest of code
        }
    }

    closedir(directory);
}

// routine
void * categorized_thread(void * arg) {
    const char * path = (const char * ) arg;
    categorized(path);
    pthread_exit(NULL);
}


int main(){
    ///   Categorize files   ///

    // buat thread baru ketika mengakses folder pertama kali pada, yaitu folder files
    pthread_t tid;
    pthread_create( & tid, NULL, categorized_thread, "./files");
    pthread_join(tid, NULL);
}
```
## Point E
Dalam setiap pengaksesan folder, pemindahan file, pembuatan folder pada program categorize.c buatlah log dengan format sebagai berikut.
DD-MM-YYYY HH:MM:SS ACCESSED [folder path]
DD-MM-YYYY HH:MM:SS MOVED [extension] file : [src path] > [folder dst]
DD-MM-YYYY HH:MM:SS MADE [folder name]
examples : 
02-05-2023 10:01:02 ACCESSED files
02-05-2023 10:01:03 ACCESSED files/abcd
02-05-2023 10:01:04 MADE categorized
02-05-2023 10:01:05 MADE categorized/jpg
02-05-2023 10:01:06 MOVED jpg file : files/abcd/foto.jpg > categorized/jpg
Catatan:
•	Path dimulai dari folder files atau categorized
•	Simpan di dalam log.txt
•	ACCESSED merupakan folder files beserta dalamnya
•	Urutan log tidak harus sama
```C
// dapatkan current time
void getCurrentTime(char * timeString) {
    time_t curTime;
    struct tm * timeInfo;

    curTime = time(NULL);

    timeInfo = localtime( & curTime);
    strftime(timeString, 20, "%d-%m-%y %H:%M:%S", timeInfo);
    // printf("%s\n", timeString);

}

// log untuk tiap accessed folder dan sub folder
void logAccessed(const char * path) {
    char timeString[20];
    getCurrentTime(timeString);

    char logCommand[100];
    logCommand[0] = '\0';
    
    strcat(logCommand, timeString);
    strcat(logCommand, " ACCESSED ");
    strcat(logCommand, path);

    char command[200];
    command[0] = '\0';
    
    strcat(command, "echo \"");
    strcat(command, logCommand);
    strcat(command, " \" >> log.txt");
    
    system(command);

}

// log saat memindahkan file
void logMoved(char * ext, char * source, char * dest) {
    char timeString[20];
    getCurrentTime(timeString);

    char logCommand[200];
    logCommand[0] = '\0';

    strcat(logCommand, timeString);
    strcat(logCommand, " MOVED ");
    strcat(logCommand, ext);
    strcat(logCommand, " file : ");
    strcat(logCommand, source);
    strcat(logCommand, " > ");
    strcat(logCommand, dest);

    char command[100];
    command[0] = '\0';
    
    strcat(command, "echo \"");
    strcat(command, logCommand);
    strcat(command, " \" >> log.txt");
    
    system(command);

}

// log saat membuat folder
void logMade(char * name) {
    char timeString[20];
    getCurrentTime(timeString);

    char logCommand[100];
    logCommand[0] = '\0';

    strcat(logCommand, timeString);
    strcat(logCommand, " MADE ");
    strcat(logCommand, name);

    char command[100];
    command[0] = '\0';
    
    strcat(command, "echo \"");
    strcat(command, logCommand);
    strcat(command, " \" >> log.txt");
    
    system(command);
    
}

void createCategorizedDirectory() {
    system("mkdir categorized");
    logMade("categorized");     // log made

    system("cd categorized && mkdir other");
    logMade("other");           // log made

    for (int i = 0; i < totalExtensions - 1; i++) {
        char command[255];
        sprintf(command, "mkdir -p categorized/%s", extensions[i]);
        system(command);
        logMade(extensions[i]);   // log made
    }
}

void categorized(const char * path) {
    DIR * directory = opendir(path);

    logAccessed(path);   // log accesed

    // code 

    if (n < maxFiles - 1) {
        // code

        // printf("%s\n", command);
        system(command);
        logMoved(ext, fullPath, ext);  // log moved
    } else {

        // code

        system(cmd_mkdir);
        // printf("%s\n", cmd_mkdir);
        char folderName[20];
        folderName[0] = '\0';
        strcat(folderName, ext);
        strcat(folderName, num);
        logMade(folderName);  // log made

        // code

        // printf("%s\n", command);
        system(command);

        char temp[100];
        strcpy(temp, dest + 14);

        logMoved(ext, fullPath, temp);  // log moved
    }

    system(command);
    logMoved(ext, fullPath, "other");  // log moved
}
```
#### Point F
Untuk mengecek apakah log-nya benar, buatlah suatu program baru dengan nama logchecker.c untuk mengekstrak informasi dari log.txt dengan ketentuan sebagai berikut.
•	Untuk menghitung banyaknya ACCESSED yang dilakukan.
•	Untuk membuat list seluruh folder yang telah dibuat dan banyaknya file yang dikumpulkan ke folder tersebut, terurut secara ascending.
•	Untuk menghitung banyaknya total file tiap extension, terurut secara ascending.
```C
char folders[100][500];  // list folder
int fileCount[100];      // list jumlah file tiap folder
int folderCount = 0;     // jumlah folder

char extensions[100][500];  // list extension
int extensionCount[100];    // list jumlah file tiap extension
int totalExtension = 0;     // total extension

int main(){
    ///   Insisasi   ///
    for (int i = 0; i < 100; i++) {
        fileCount[i] = 0;
    }

    for (int i = 0; i < 100; i++) {
        extensionCount[i] = 0;
    }

}
```
Menginisialisai Array
```C
int main(){
    ///   Buka File   ///	

    FILE * fp = fopen("log.txt", "r");

    if (fp == NULL) {
        perror("Error opening file");
        exit(EXIT_FAILURE);
    }
    char buffer[200];  // buffer untuk menyimpan baris log
}
```
Membuka file
```C
int main(){
    ///   Hitung Jumlah ACCESSED   ///
    int AccessedCount = 0;
    while (fgets(buffer, 200, fp)) {
        if (strstr(buffer, "ACCESSED") != NULL) {
            AccessedCount++;
        }
    }

    printf("Jumlah 'ACCESSED': %d\n", AccessedCount);
}
```
Menghitung jumlah accessed
```C
void sort(char arr[][500], int n) {
    int i, j;
    char temp[100];
    for (i = 0; i < n - 1; i++) {
        for (j = 0; j < n - i - 1; j++) {
            if (strcmp(arr[j], arr[j + 1]) > 0) {
                strcpy(temp, arr[j]);
                strcpy(arr[j], arr[j + 1]);
                strcpy(arr[j + 1], temp);
            }
        }
    }
}

int main(){
    ///   List Folder   ///

    fp = fopen("log.txt", "r");
    char * folder;

    while (fgets(buffer, 200, fp)) {
        folder = strstr(buffer, "MADE ");

        if (folder != NULL) {
            folder += strlen("MADE ");
            folder[strlen(folder) - 2] = '\0';
            strcpy(folders[folderCount], folder);
            folderCount++;

        }
    }

    ///   Sort Folder   ///

    sort(folders, folderCount);
}
```
list dan sort folder
```C
int getFolderIndex(char * folder) {
    for (int i = 0; i < folderCount; i++) {
        if (strcmp(folder, folders[i]) == 0) {
            return i;
        }
    }

    return -1;
}

int main(){
    ///   Hitung Jumlah File Tiap Folder   ///

    fp = fopen("log.txt", "r");
    char temp[100];

    while (fgets(buffer, 200, fp)) {
        if (folder = strstr(buffer, "> ")) {
            folder[strlen(folder) - 2] = '\0';
            strcpy(temp, folder + 2);

            int folderIndex = getFolderIndex(temp);

            fileCount[getFolderIndex(temp)]++;

        }

    }

}
```
Menghitung jumlah file tiap folder
```C
int main(){
    ///   List Extension   ///

    char * extension;
    fp = fopen("log.txt", "r");
    while (fgets(buffer, 200, fp)) {
        if (extension = strstr(buffer, "MOVED ")) {
            strcpy(temp, extension + 6);
            strcpy(extension, temp);

            if (extension[0] == ' ') {
                strcpy(extension, " ");
            } else {
                char * temp2 = strtok(extension, " ");
                strcpy(extension, temp2);

            }

            int extensionExist = 0;
            for (int i = 0; i < totalExtension; i++) {
                if (strcmp(extension, extensions[i]) == 0) {
                    extensionExist = 1;
                }
            }

            if (extensionExist == 0) {

                strcpy(extensions[totalExtension], extension);
                totalExtension++;

            }

        }
    }

    ///   Sort Extension   ///

    sort(extensions, totalExtension);
}
```
List dan sort tiap extension
```C
int getExtensionIndex(char * extension) {
    for (int i = 0; i < totalExtension; i++) {
        if (strcmp(extension, extensions[i]) == 0) {
            return i;
        }

    }
    return -1;

}

int main(){
    ///   Hitung Jumlah File Tiap Extension   ///

    fp = fopen("log.txt", "r");
    while ((fgets(buffer, 200, fp))) {
        if (extension = strstr(buffer, "MOVED ")) {
            strcpy(temp, extension + 6);
            strcpy(extension, temp);

            if (extension[0] == ' ') {
                strcpy(extension, " ");
            } else {
                char * temp2 = strtok(extension, " ");
                strcpy(extension, temp2);

            }

            int extensionIndex = getExtensionIndex(extension);

            extensionCount[extensionIndex]++;

        }
    }
}
```
Menghitung jumlah file tiap extension
```C
Print Hasilnya



---

## Endnote
Banyak kekurangan dan kesalahan dalam pengerjaan laporan ini, mohon dimaklumi.
