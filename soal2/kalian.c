//no.2
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <unistd.h>

int Aarr[4][2], Barr[2][5], Carr[4][5];
int Bnum, Anum;


void isi_array(){
    for(int i = 0; i < 4; i++){
        for(int j = 0; j < 2; j++){
            Anum = (rand() % 5 + 1);
            Aarr[i][j] = Anum;
        }
    }
    for (int i = 0; i < 2; i++){
        for(int j = 0; j < 5; j++){
            Bnum = (rand() % 4 + 1);
            Barr[i][j] = Bnum;
        }
    }
}

void cross_product() {
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            Carr[i][j] = Aarr[i][0] * Barr[0][j] + Aarr[i][1] * Barr[1][j];
        }
    }
}

int main(){
    srand(time(NULL));
    isi_array();

    // cek Barr dan Aarr
    for(int i=0; i<4; i++){
    printf("%d %d\n", Aarr[i][0], Aarr[i][1]);
    }   
    for(int i=0; i<2; i++){
    printf("%d %d %d %d %d\n", Barr[i][0], Barr[i][1], Barr[i][2], Barr[i][3], Barr[i][4]);
    }
    printf("\n");

    // mengkalikannya 
    cross_product();

    //print Carr
    for (int i = 0; i < 4; i++)
    {
        for (int j = 0; j < 5; j++)
        {
            printf("%d ", Carr[i][j]);
        }
        printf("\n");
    }

    /* Allocate shared memory segment */
    key_t key = 1928;
    const int shareSize = sizeof(int) * 4 * 5;  // size of Carr array
    int segmentId = shmget(key, shareSize, IPC_CREAT | 0666);

    /* Attach the shared memory segment */
    int *sharedCarr = (int *)shmat(segmentId, NULL, 0);

    /* Copy the contents of Carr to shared memory */
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            sharedCarr[i * 5 + j] = Carr[i][j];
        }
    }

    /* Rest of your code... */
	sleep(5);
    /* Detach shared memory segment */
    shmdt(sharedCarr);
    /* Remove shared memory segment */
    shmctl(segmentId, IPC_RMID, NULL);

    return 0;
    

}
