#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <pthread.h>
#include <time.h>

struct param_t {
	int num;
	unsigned long long result;
};

unsigned long long factorial(unsigned long long n){
    if (n >=1)
    	return n * factorial(n-1);
    else
    	return 1;
}

void *thread_func(void *args){
    struct param_t *param = (struct param_t *)args;
    param->result = factorial(param->num);
    return (void *)NULL;
}

int main() {


	//clock to count time used
    clock_t start, end;
    double cpu_time_used;
	start = clock();

    int segmentId;
    int *sharedCarr;
    key_t key = 1928;     

    /* Allocate shared memory segment */
    const int shareSize = sizeof(int) * 4 * 5;  // size of Carr array
    segmentId = shmget(key, shareSize, IPC_CREAT | 0666);

    /* Attach the shared memory segment */
    sharedCarr = (int *)shmat(segmentId, NULL, 0);

    /* Read the contents of shared memory into a local array */
    int Carr[4][5];
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            Carr[i][j] = sharedCarr[i * 5 + j];
        }
    }

    /* Print the contents of Carr */
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            printf("%d ", Carr[i][j]);
        }
        printf("\n");
    }
    int rows = sizeof(Carr) / sizeof(Carr[0]);
    int cols = sizeof(Carr[0]) / sizeof(Carr[0][0]);
    pthread_t threads[rows][cols];
    struct param_t params[rows][cols];
    unsigned long long fact[rows][cols];
    
    for (int i=0; i<rows; i++) {
    	for (int j =0; j < cols; j++) {
    	   params[i][j].num = Carr[i][j];
    	   pthread_create(&threads[i][j], NULL, thread_func, &params[i][j]);
    	}
    }
    
    for (int i=0; i<rows; i++){
    	for(int j=0; j<cols; j++){
    	   pthread_join(threads[i][j], NULL);
    	   fact[i][j] = params[i][j].result;
    	   printf("%lld ", fact[i][j]);
       }
    }
	
    /* Detach shared memory segment */
    shmdt(sharedCarr);
    /* Remove shared memory segment */
    shmctl(segmentId, IPC_RMID, NULL);
	
	//clock to count time
	end = clock();
	cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
	printf("\nMulti-threading took %f seconds \n", cpu_time_used);
	
    return 0;
}
